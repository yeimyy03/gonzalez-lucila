package modulo3y4;
import java.util.Scanner;
public class Ejercicio13 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingrese un mes por su nombre  ");
		String mes = scan.nextLine();
		
		switch(mes){
		case "febrero": case "Febrero":
			System.out.println(mes+" tiene 28 dias");
			break;
		case "abril": case "Abril":
		case "junio": case "Junio":
		case "septiembre": case "Septiembre":
		case "Noviembre": case "noviembre":
			System.out.println(mes+" tiene 30 d�as");
			break;
		case "enero": case "Enero":
		case "marzo": case "Marzo":
		case "mayo": case "Mayo":
		case "julio": case "Julio":
		case "agosto": case "Agosto":
		case "octubre": case "Octubre":
		case "diciembre": case "Diciembre":
			System.out.println(mes+" tiene 31 d�as");
			break;
		default:
			System.out.println("Mes incorrecto");
		}
		scan.close();
	}
}
