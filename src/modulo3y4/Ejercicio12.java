package modulo3y4;
import java.util.Scanner;
public class Ejercicio12 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
				
		System.out.print("Elegir una letra del abecedario  ");
		char let = scan.next().charAt(0);
		
		if(let=='a'||let=='e'||let=='i'||let=='o'||let=='u') {System.out.println("Esa letra es una vocal");}
		else {System.out.println("Esa letra es una consonante");}
		scan.close();
	}

}
