package modulo3y4;
import java.util.Scanner;
public class Ejercicio14 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
				
		System.out.print("�Qu� puesto obtuvo? (Expresar en palabras)  ");
		String place = scan.nextLine();
		
		switch(place) {
		case "primero": case "Primero":
			System.out.println("Obtiene la medalla de oro");
			break;
		case "segundo": case "Segundo":
			System.out.println("Obtiene la medalla de plata");
			break;
		case "tercero": case "Tercero":
			System.out.println("Obtiene la medalla de bronce");
			break;
		default:
			System.out.println("Siga participando");
		}
		scan.close();
	}
}