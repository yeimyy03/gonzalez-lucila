/**
 * 
 */
package modelo.componentes;

/**
 * Esta clase va a agrupar las caracteristicas de un inductor y su comportamiento
 * @author Gabriel
 *
 */


public class Inductor extends Componente {	
	private int frecuencia;
	
	//constructores
	
	public Inductor(){
		//se le agregan valor por defecto
		super("Ldef", 0.001f, "Hy");
		frecuencia = 1000;
	}
	public Inductor(String pNom, float pValor, int pFcia){
		super(pNom, pValor, "Hy");
		this.frecuencia = pFcia;		
	}

	//nedodos de negocio
	public float calcularImpedancia(){
		return (float)(2*Math.PI* this.frecuencia * getValor());
	}
		
	//accesor
	public int getFrecuencia() {					return frecuencia;				}
	public void setFrecuencia(int frecuencia) {		this.frecuencia = frecuencia;	}
	//metods especiales, y felices
	
	
   public boolean equals(Object obj){
	   boolean bln = false;
	  
	   if(obj!=null && obj instanceof Inductor){
		   Inductor indu = (Inductor)obj;
		   bln = super.equals(indu) 				&&
				 indu.getFrecuencia() == frecuencia;  
	   }
	   return bln;
	   
   }
   public int hashCode(){
	   return super.hashCode() + frecuencia;
   }
   
   public String toString(){
	   StringBuilder sb = new StringBuilder(super.toString());	
	   sb.append(", frecuencia=");
	   sb.append(frecuencia);
   
	   return sb.toString();
   }
  }
	
	
	

